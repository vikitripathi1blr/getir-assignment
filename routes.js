const router = require('express').Router();

router.use('/records', require('./api/records/records.router'));


module.exports = {
    router: router
}