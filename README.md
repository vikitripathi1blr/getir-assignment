# Getir API Assignment

### Base API Endpoint
```
https://secret-bayou-98747.herokuapp.com/
```

### Project setup
```
npm install
npm run start:prod
```

### Swagger Documentation
```
https://secret-bayou-98747.herokuapp.com/api-docs/#/
```

### Run Tests
```
npm run test
```
