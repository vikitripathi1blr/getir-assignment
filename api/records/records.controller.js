const recordService = require("./records.service");
const recordResponse = require("./records.payload");

const getRecordsWithCountResponse = recordResponse.getRecordsWithCountResponse;


/*
 * @body {Object} - { "startDate" : "YYYY-MM-DD", "endDate": "YYYY-MM-DD", "minCount": Int, "maxCount": Int }
 * @returns {Object} - List of filtered Records { "key": String, "createdAt": Date String, "totalCount": Int}
 * totalCount - sum of the “counts”  array in the document
 */
const getlist = async function(req, res) {
  const categoryData = await recordService.getRecordsWithCount(req, res);

  return res.status(200).send(getRecordsWithCountResponse(categoryData));
};


module.exports = {
  getlist,
};
