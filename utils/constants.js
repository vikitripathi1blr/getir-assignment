
const statusCodes = {
  Success: 0,
  RecordsUnavailable: 101,
  UnprocessableEntity: 102,

};

const statusMessages = {
  Success: "Success",
  RecordsUnavailable: "Some internal error ocurred",
  UnprocessableEntity: "Mandatory fields not in correct format",
};

module.exports = {
  code: statusCodes,
  statusMessages: statusMessages,
}
