module.exports = {
  components: {
    schemas: {
      // Records model
      Records: {
        type: "object", // data type
        properties: {
          key: {
            type: "string", 
            description: "Records's key", 
            example: "sadsa",
          },
          value: {
            type: "string", 
            description: "Records's value", 
            example: "asdadsas",
          },
          createdAt: {
            type: "date", 
            description: "Records date", 
            example: "2017-01-27T08:19:14.135Z",
          },
          counts: {
            type: "number", 
            description: "counts list", 
            example: [1,2,3], 
          },
        },
      },
      // Records Filter
      RecordsFilter: {
        type: "object", // data type
        properties: {
          startDate: {
            type: "string", 
            description: "Records's createdAt greater than or equal to", 
            example: "2017-01-27",
          },
          endDate: {
            type: "string", 
            description: "Records's createdAt less than  or equal to", 
            example: "2017-01-27",
          },
          minCount: {
            type: "number", 
            description: "Records min count", 
            example: 8,
          },
          maxCount: {
            type: "number", 
            description: "Records max count", 
            example: 15, 
          },
        },
      },
      // error model
      Error: {
        type: "object", //data type
        properties: {
          message: {
            type: "string", // data type
            description: "Error message", // desc
            example: "Not found", // example of an error message
          },
          internal_code: {
            type: "string", // data type
            description: "Error internal code", // desc
            example: "Invalid parameters", // example of an error internal code
          },
        },
      },
    },
  },
};
