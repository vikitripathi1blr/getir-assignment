module.exports = {
  // operation's method
  post: {
    tags: ["Records Filter List"], // operation's tag
    description: "Get Records", // short desc
    operationId: "getRecords", // unique operation id
    parameters: [], // expected params
    requestBody: {
      // expected request body
      content: {
        // content-type
        "application/json": {
          schema: {
            $ref: "#/components/schemas/RecordsFilter", // todo input data model
          },
        },
      },
    },
    // expected responses
    responses: {
      // response code
      200: {
        description: "Records fetched successfully", // response desc
      },
      // response code
      500: {
        description: "Server error", // response desc
      },
    },
  },
};
