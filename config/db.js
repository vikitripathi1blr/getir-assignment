const mongoose = require("mongoose");

const DB_URI = process.env.MONGO_URI;

const configOptions = {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true};

const connectDb = async () => {
  try {
    await mongoose.connect(DB_URI, configOptions)

    console.info(`Connected to database on Worker process: ${process.pid}`);
  } catch (error) {
    console.log(`Connection error: ${error.stack} on Worker process: ${process.pid}`);
    process.exit(1)
  }
}

module.exports = {
  connectDb: connectDb,
}
